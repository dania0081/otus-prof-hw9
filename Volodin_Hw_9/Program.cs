﻿using System;

// Базовый класс
class BaseClass
{
    public string Name { get; set; }

    public BaseClass(string name)
    {
        Name = name;
    }
}

// Производный класс, наследуется от BaseClass
class DerivedClass1 : BaseClass, IMyCloneable<DerivedClass1>
{
    public DerivedClass1(string name) : base(name)
    {
    }

    public DerivedClass1 MyClone()
    {
        return new DerivedClass1(Name);
    }
}

// Производный класс, наследуется от DerivedClass1
class DerivedClass2 : DerivedClass1, IMyCloneable<DerivedClass2>, ICloneable
{
    public DerivedClass2(string name) : base(name)
    {
    }

    public new DerivedClass2 MyClone()
    {
        return new DerivedClass2(Name);
    }

    public object Clone()
    {
        return MemberwiseClone();
    }
}

// Производный класс, наследуется от DerivedClass2
class DerivedClass3 : DerivedClass2, IMyCloneable<DerivedClass3>, ICloneable
{
    public DerivedClass3(string name) : base(name)
    {
    }

    public new DerivedClass3 MyClone()
    {
        return new DerivedClass3(Name);
    }

    public object Clone()
    {
        return MemberwiseClone();
    }
}

// Дженерик интерфейс IMyCloneable
interface IMyCloneable<T>
{
    T MyClone();
}

class Program
{
    static void Main(string[] args)
    {
        // Создание объектов и клонирование
        var obj1 = new DerivedClass1("Object 1");
        var clone1 = obj1.MyClone();

        var obj2 = new DerivedClass2("Object 2");
        var clone2 = obj2.Clone();

        var obj3 = new DerivedClass3("Object 3");
        var clone3 = obj3.Clone();

        // Вывод результатов
        Console.WriteLine($"Original: {obj1.Name}, Clone: {clone1.Name}");
        Console.WriteLine($"Original: {obj2.Name}, Clone: {((DerivedClass2)clone2).Name}");
        Console.WriteLine($"Original: {obj3.Name}, Clone: {((DerivedClass3)clone3).Name}");

        // Проверка клонирования объектов
        Console.WriteLine($"obj1 == clone1: {ReferenceEquals(obj1, clone1)}");
        Console.WriteLine($"obj2 == clone2: {ReferenceEquals(obj2, clone2)}");
        Console.WriteLine($"obj3 == clone3: {ReferenceEquals(obj3, clone3)}");

        Console.WriteLine("False - отдельные объекты");
    }
}